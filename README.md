# public-etic-sftgo


## generate certificate
```bash
cd ./certificates/
sudo chmod +x generate_certificates.sh
./generate_certificates.sh
```

# create docker network 
to allow other containers in the same server to access ftp  
```
docker network create --gateway 192.168.10.1 --subnet 192.168.10.0/24 sftpgo-debug
```

# Update env file
Set this env variable in the file .env  
**PASSIVE_IP_PUBLIC_OVERRIDE**

Other variables could be updated but not necessarily

# start container
```
docker compose up -d
```

# connect to admin url
```
https://IP:PORT/web/admin/login
```

# Some users are already created 
file: config/sftpgo_user_accounts.json 

## user admin 
username: admin  
password: industrie  

## user ftp
username: user  
password: industrie  


# Use Filezilla for a client ftp (use passiv mode)
- Connect with user ftp  
- Transfer file  
- The file should be store in ./data  


## Config file
The sftpgo config file is available in config/sftpgo.json  
Some variables are dynamic injected in the config :  
- SFTPGO_FTPD__PASSIVE_PORT_RANGE__START
- SFTPGO_FTPD__PASSIVE_PORT_RANGE__END
- SFTPGO_FTPD__BINDINGS__0__FORCE_PASSIVE_IP
- SFTPGO_FTPD__BINDINGS__0__PASSIVE_IP_OVERRIDES__0__NETWORKS
- SFTPGO_FTPD__BINDINGS__0__PASSIVE_IP_OVERRIDES__0__IP
