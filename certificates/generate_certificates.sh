# Création d'une autorité de certification

## Génerer private key CA
openssl genrsa -out ca.key 2048

## Génération certificat 
openssl req -new -x509 -key ca.key -out ca.crt -days 3000 -subj "/OU=Domain Control Validated/CN=ca.powerop.io"


#Création certificat serveur
## Génénérate private key serveur
openssl genrsa -out server.key 2048

## Création d'une demande de certificat serveur
openssl req -new -key server.key -out server.csr -subj "/OU=Domain Control Validated/CN=ftps.powerop.io"

## Génération du certificat final validé par le CA
openssl x509 -req -in server.csr -out server.crt -CA ca.crt -CAkey ca.key -CAcreateserial -CAserial ca.srl -days 3000


#Création certificat client
## Génerer private key client
openssl genrsa -out client.key 2048

## Création d'une demande de certificat client
openssl req -new -key client.key -out client.csr -subj "/OU=Domain Control Validated/CN=client.powerop.io"

## Génération du certificat final validé par le CA
openssl x509 -req -in client.csr -out client.crt -CA ca.crt -CAkey ca.key -CAcreateserial -CAserial ca.srl -days 3000